import React from 'react';
import {uuid} from "./extra";

export default class App extends React.Component {
    constructor(props){
        super(props);
        this.state={
            text: "",
            items: [],
            filter: "all"
        };
        this.onSubmit=this.onSubmit.bind(this);
        this.onChange=this.onChange.bind(this);
        this.onDelete=this.onDelete.bind(this);
        this.onCheck=this.onCheck.bind(this);
        this.onClearComplited=this.onClearComplited.bind(this);
        this.show=this.show.bind(this);
    }

    onSubmit(event){
       event.preventDefault();
       const newItem ={
         text: this.state.text,
           id: uuid(),
           done: ""
       };

       this.setState((prevState)=>({
           items: prevState.items.concat(newItem),
           text:""
       }));
    }

    onChange(event){
        this.setState({text: event.target.value});
    }

    onDelete(id){
        this.setState((prevState)=>({
            items: prevState.items.filter(item => item.id !== id)
        }));
    }

    onCheck(id){
        this.state.items.map(function (item) {
            if (item.id == id) {
                item.done = (item.done ? "" : "done");
            }
        });
        this.setState({items: this.state.items})
    }

    onClearComplited(){
        this.setState((prevState)=>({
            items: prevState.items.filter(item => item.done == ""),
            filter: "all"
        }));
    }

    show(stat){
        this.setState({filter: stat});
    }

    render(){
        return(
            <div>
                <h1>TODOS</h1>
                <div className="content">
                    <form onSubmit={this.onSubmit}>
                        <input
                            placeholder="add todo"
                            autoFocus={true}
                            onChange={this.onChange}
                            value={this.state.text}
                        />
                    </form>
                    <TodoList
                        filter={this.state.filter}
                        items={this.state.items}
                        onDelete={this.onDelete}
                        onCheck={this.onCheck}
                    />
                    <TodoHandleMenu
                        show={this.show}
                        items={this.state.items}
                        filter={this.state.filter}
                        onClearComplited={this.onClearComplited}
                    />
                </div>
            </div>
        )
    }
}

class TodoList extends React.Component{

    onDelete(id){
        this.props.onDelete(id);
    }

    onCheck(id){
        this.props.onCheck(id)
    }

    render(){
        const items = this.props.items;

        return(
            <ul>
                {items.filter(items => items.done !== this.props.filter ).map(item =>(
                    <li className={item.done} key={item.id}>
                        <input
                            onClick={this.onCheck.bind(this, item.id)}
                            type="checkbox"
                            defaultChecked={item.done == "done" ? true : false}
                        />
                        <label>{item.text}</label>
                        <div className="spacer"></div>
                        <button className="button"
                            onClick={this.onDelete.bind(this, item.id)}
                        >X</button>
                   </li>
                ))}
            </ul>
        );
    }
}

class TodoHandleMenu extends React.Component {
    constructor(props){
        super(props);
    }

    onClearComplited() {
        this.props.onClearComplited()
    }

    show(stat){
        this.props.show(stat)
    }

    renderMenu(){
        if(this.props.items.length>0) {
            const doneCount = this.props.items.reduce((count, item) => {
                return item.done == "done" ? count + 1 : count
            }, 0);

            const undoneCount = (this.props.items.length) - doneCount;

            const itemsUndone = undoneCount === 1 ? "item" : "items";

            return (
                <div className="handleMenu">
                    <span>{undoneCount} {itemsUndone} left</span>
                    <section>
                        <a
                            className={"filtre" + this.props.filter}
                            onClick={this.show.bind(this, "all")}
                        >All</a>
                        <a
                            className={"active" + this.props.filter}
                            onClick={this.show.bind(this, "done")}
                        >Active</a>
                        <a
                            className={"complit" + this.props.filter}
                            onClick={this.show.bind(this, "")}
                        >Complited</a>
                    </section>
                    <button onClick={this.onClearComplited.bind(this)}>
                        Clear Complited
                    </button>
                </div>
            )
        }
    }

    render(){
        return (
            <section>
                {this.renderMenu()}
            </section>
        )
    }
}