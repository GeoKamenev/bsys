"use strict";

if (chrome.browserAction !== undefined) {
    chrome.browserAction.onClicked.addListener(onButtonClick);
}

function onButtonClick() {
    window.open("https://google.ru/");
}

var observer = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
        if (!mutation.addedNodes) return
        for (var i = 0; i < mutation.addedNodes.length; i++) {
            var node = mutation.addedNodes[i];
            if (node.nodeName == "HEAD") {
                var injectScript = document.createElement('script');
                injectScript.setAttribute("type", "text/javascript");
                injectScript.setAttribute("src", "__");
                document.head.insertBefore(injectScript, document.head.firstChild);
                observer.disconnect();
            }
        }
    })
})

observer.observe(document, {
    childList: true,
    subtree: true
})
