"use strict";

window.addEventListener("load", init);

function init() {
    var xhr = new XMLHttpRequest();
    var maxPrice;
    var minPrice;
    showGroups();
    autoComplete ();
    getMinMaxPrice();

    function productsInnerHTML(data) {
        var productL = data.length;
        document.querySelector("#products").innerHTML = "";
        for (var i = 0; i < productL; i++) {
            document.querySelector("#products").innerHTML += "<div class='row'><div class='col-md-8 col-sm-8'>" + data[i].name
                + "</div><div class='col-md-4 col-sm-8'>" + data[i].price + "</div></div>";
        }
    }

    function showGroups() {

        xhr.open('GET', 'ajax.php?action=get_groups', false);

        xhr.send();

        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            var groups = JSON.parse(xhr.responseText);
            var groupsL = groups.length;
            for (var i = 0; i < groupsL; i++) {
                document.querySelector("#groups").innerHTML += "<li><a href='#' data-group='" + groups[i].id + "'>" + groups[i].name + "</a></li>";
            }
        }

        document.querySelectorAll("#groups a").forEach(function (a) {
            a.addEventListener("click", clickGroups);
        });
    }

    function clickGroups(e) {
        e.preventDefault();
        var group = this.dataset["group"];
        xhr.open('GET', 'ajax.php?action=get_products&group=' + group, false);
        xhr.send();
        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            var products = JSON.parse(xhr.responseText);
            productsInnerHTML(products);
        }

    }

    function autoComplete() {
        $("#search").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "ajax.php",
                    data: {
                        action: "get_list",
                        name: request.term
                    },
                    dataType: "json",
                    success: function (data) {
                        response($.map(data, function (item) {
                            return item.name;
                        }));
                    }
                })
            }
        });
    }

    function getMinMaxPrice() {
        xhr.open('GET', 'ajax.php?action=get_products', false);
        xhr.send();
        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            var products = JSON.parse(xhr.responseText);
            var productL = products.length;
            maxPrice = 0;
            for (var i = 0; i < productL; i++) {
               if (parseFloat(products[i].price) > maxPrice){
                   maxPrice = parseFloat(products[i].price);
               }
            }

            $("input#maxCost").val(maxPrice);

            minPrice = maxPrice;
            for (var i = 0; i < productL; i++) {
                if (parseFloat(products[i].price) < minPrice){
                    minPrice = parseFloat(products[i].price);
                }
            }
            $("input#minCost").val(minPrice);
        }
    }

    $("#slider").slider({

        min: minPrice,
        max: maxPrice,
        values: [minPrice, maxPrice],
        range: true,
        stop: function (event, ui) {
            productsMinMax(ui.values[0], ui.values[1]);
        },
        slide: function (event, ui) {
            $("input#minCost").val($("#slider").slider("values", 0));
            $("input#maxCost").val($("#slider").slider("values", 1));
        }
    });

    $("input#minCost").change(function () {

        var value1 = $("input#minCost").val();
        var value2 = $("input#maxCost").val();

        if (parseInt(value1) > parseInt(value2)) {
            value1 = value2;
            $("input#minCost").val(value1);
        }
        $("#slider").slider("values", 0, value1);

        productsMinMax(value1, value2);
    });

    $("input#maxCost").change(function () {

        var value1 = $("input#minCost").val();
        var value2 = $("input#maxCost").val();

        if (value2 > maxPrice) {
            value2 = maxPrice;
            $("input#maxCost").val(maxPrice)
        }

        if (parseInt(value1) > parseInt(value2)) {
            value2 = value1;
            $("input#maxCost").val(value2);
        }
        $("#slider").slider("values", 1, value2);

        productsMinMax(value1, value2);
    });

    function productsMinMax(value1, value2) {
		$.ajax({
			url: "ajax.php",
			data: {
				action: "get_products",
				min: value1,
				max: value2
			},
			dataType: "json",
			success: function (data) {
				productsInnerHTML(data);
			}
		});
        
    }

}