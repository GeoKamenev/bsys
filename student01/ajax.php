<?php
$mysqli = new mysqli("localhost", "root", "1ktfl0", "avalon");
if ($_GET['action'] == 'get_groups') {
    $res = $mysqli->query("SELECT * FROM groups");
    $groups = [];
    while ($row = $res->fetch_assoc()) {
        $groups[] = $row;
    }
    echo json_encode($groups);
} elseif ($_GET['action'] == 'get_products') {

    $arWhere = [];
    if (isset($_GET['min'])) {
        $arWhere[] = "price >= " . $_GET['min'];
    }
    if (isset($_GET['max'])) {
        $arWhere[] = "price <= " . $_GET['max'];
    }
    if (isset($_GET['group'])) {
        $arWhere[] = "products.group={$_GET['group']}";
    }

    if (count($arWhere) > 0) {
        $where = " WHERE " . implode(" AND ", $arWhere);
    } else {
        $where = "";
    }

    $res = $mysqli->query("SELECT * FROM products{$where}");

    $products = [];
    while ($row = $res->fetch_assoc()) {
        $products[] = $row;
    }
    echo json_encode($products);

} elseif ($_GET['action'] == 'get_list') {
    if (isset($_GET['name'])) {
        $res = $mysqli->query(
            "SELECT * FROM `products` WHERE `name` LIKE '%{$_GET['name']}%' LIMIT 10");
        $products = [];
        while ($row = $res->fetch_assoc()) {
            $products[] = $row;
        }
        echo json_encode($products);
    }
}