
import {uuid} from "../extra/uuid";


function todos(state = [], action) {
    switch (action.type){
        case 'ADD_TODO':
            return [
                ...state,
                {
                    id: uuid(),
                    completed: 'uncompleted',
                    text: action.text
                }
            ];

        case 'DELETE_TODO':
            return state.filter(todo =>
                todo.id !== action.id
            );

        case 'COMPLETE_TODO':
            return state.map(todo =>
                todo.id === action.id ? {...todo, completed: (
                    todo.completed !== "uncompleted" ? 'uncompleted' : 'completed'
                )} : todo
            );

        case 'CLEAR_COMPLETED':
            return state.filter(todo =>
                todo.completed !== 'completed'
            );

        default:
            return state
    }
}

export default todos;