import {combineReducers} from 'redux';
import filterTodos from './filters';
import todos from './todos'

const Reducer = combineReducers({
    todos,
    filterTodos
});

export default Reducer;