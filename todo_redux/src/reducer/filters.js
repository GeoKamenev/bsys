

function filterTodos (state = 'all', actions){
    switch (actions.type){
        case 'SWITCH_ALL':
            return state = 'all';

        case 'SWITCH_UNCOMPLETE':
            return state = 'completed';

        case 'SWITCH_COMPLETE':
            return state = 'uncompleted';

        default:
            return state
    }
}

export default filterTodos;