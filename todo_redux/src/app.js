import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import {AddTodo, TodoList, TodoMenu} from './components/TodoComponents';
import * as Actions from './actions';

const App = ({todos, filterTodos, actions}) => {
    return(
        <div>
            <h1>TODO</h1>
            <div className='content'>
                <AddTodo actions={actions}/>
                <TodoList todos={todos} filterTodos={filterTodos} actions={actions}/>
                <TodoMenu todos={todos} filterTodos={filterTodos} actions={actions}/>
            </div>
        </div>

    )
};

const mapStateToProps = state => ({
    todos: state.todos,
    filterTodos: state.filterTodos
});

const mapDispatchToProps = dispatch => ({
   actions: bindActionCreators(Actions, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);