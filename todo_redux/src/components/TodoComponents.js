import React from 'react';

export class AddTodo extends React.Component{

    state = {
        text:''
    };

    onSubmit = (event) => {
      event.preventDefault();
      const text = this.state.text;
      this.props.actions.newTodo(text);
      this.setState({text:''});
    };

    onChange = (event) => {
        this.setState({text: event.target.value})
    };

    render(){
        return(
            <div>
                <form onSubmit={this.onSubmit}>
                    <input
                        type='text'
                        placeholder='add todo'
                        value={this.state.text}
                        autoFocus={true}
                        onChange={this.onChange}
                    />
                </form>
            </div>
        )
    }
}

export class TodoList extends React.Component{

    render(){
        const {todos, filterTodos} = this.props;
        const { deleteTodo, completeTodo}= this.props.actions;
        return(
            <ul>
                {todos.filter(todo =>
                    todo.completed !== filterTodos).map(todo =>(
                    <li className={todo.completed} key={todo.id}>
                        <input
                            type='checkbox'
                            defaultChecked={todo.completed !== 'uncompleted' ? true : false}
                            onClick={()=>completeTodo(todo.id)}
                        />
                        <label>{todo.text}</label>
                        <div className="spacer"></div>
                        <button className="button"
                            onClick={()=>deleteTodo(todo.id)}
                        >X</button>
                    </li>
                ))}
            </ul>
        )
    }
}

export class TodoMenu extends React.Component{

    handleMenu(){
        const {todos, filterTodos} = this.props;
        const {clearCompleted, switchAll, switchUncomplete, switchComplete} = this.props.actions;

        if(todos.length>0){
            const completedCount = todos.reduce((count, todo) => {
                return todo.completed ? count+1 : count
            },0);

            const uncompletedCount = todos.length - completedCount;

            const itemsUncompleted = uncompletedCount === 1 ? "item" : "items";

            return(
                <div className="handleMenu">
                    <span>{uncompletedCount} {itemsUncompleted} left</span>
                    <section>
                        <a
                            className={filterTodos}
                            onClick={()=>switchAll()}
                        >All</a>
                        <a
                            className={filterTodos}
                            onClick={()=>switchUncomplete()}
                        >Active</a>
                        <a
                            className={filterTodos}
                            onClick={()=>switchComplete()}
                        >Complited</a>
                    </section>
                    <button onClick={()=>clearCompleted()}>
                        Clear Completed
                    </button>
                </div>
            )
        }
    }

    render(){
        return(
            <div>
                {this.handleMenu()}
            </div>
        )
    }
}