
export const newTodo = (text) => {
    return {
        type: 'ADD_TODO',
        text
    }
};

export const deleteTodo = (id) => {
    return {
        type: 'DELETE_TODO',
        id
    }
};

export const completeTodo = (id) => {
    return {
        type: 'COMPLETE_TODO',
        id
    }
};

export const clearCompleted = () => {
    return {
        type: 'CLEAR_COMPLETED'
    }
};

export const switchAll = () => {
    return {
        type: 'SWITCH_ALL'
    }
};

export const switchUncomplete = () => {
    return {
        type: 'SWITCH_UNCOMPLETE'
    }
};

export const switchComplete = () => {
    return {
        type: 'SWITCH_COMPLETE'
    }
};