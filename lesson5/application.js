"use strict";

window.addEventListener("load", init);

function init() {
    showTab("#control");

    var sumP = people.length;
    document.getElementById("NumberOfStudents").innerHTML = "Общее количество: " + sumP + " человек.";

    var groups = [];
    var maxAgAll;
    var minAgAll;
    var maxName;
    var minName;
    minAgAll = maxAgAll = people[1].age;

    for (var i = 0; i < people.length; i++) {
        if (people[i].age > maxAgAll) {
            maxAgAll = people[i].age;
            maxName = people[i].name.first + " " + people[i].name.last;
        } else if (people[i].age < minAgAll) {
            minAgAll = people[i].age;
            minName = people[i].name.first + " " + people[i].name.last;
        }

        if (groups.indexOf(people[i].group) === -1) {
            groups.push(people[i].group);
        }
    }

    groups = groups.sort(function (a, b) {
        return a - b;
    });
    console.log(groups);

    showAllGroups();
    hideAllGroups();

    function showAllGroups() {
        var liGroups = "<li data-group='all'><a>Все группы</a></li>";
        var nptGroup;
        for (var i = 0; i < groups.length; i++) {
            liGroups += "<li data-group='" + (groups[i]) + "'><a>Группа " + groups[i] + "</a></li>";
            nptGroup = groups[i];
        }
        document.querySelector("#groupType>ul").innerHTML = liGroups;

        document.querySelector("#selNumGroup").innerHTML =  "<label for='group'>Группа</label><input type='number' min='"
            + groups[0]+ "' max = '" + nptGroup + "' class='form-control' id='group'>";

        document.querySelector("#CreateGroup").addEventListener("click", newGroup);

    }
    // добавление группы
    function newGroup() {
        var ng = groups.length;
        ng++;
        groups.push(ng);
        showAllGroups();
        hideAllGroups();
    }


    // проверка на пустые группы
    function hideAllGroups() {
        document.querySelectorAll("#groupType>ul li").forEach(function (el) {
            el.classList.add("hidden");
        });
        document.querySelector("[data-group='all']").classList.remove("hidden");
        for (var i=0; i<groups.length; i++) {
            for (var j = 0; j < people.length; j++) {
                if (groups[i] == people[j].group){
                    document.querySelector("[data-group='" + (groups[i]) + "']").classList.remove("hidden");
                    break;
                }
            }
        }
    }

    document.getElementById("NumberOfGroup").innerHTML = groups.length + " групп.";

    document.querySelector("#submitStudentForm").addEventListener("submit", function (e) {

        e.preventDefault();

        function guid() {
            function _p8(s) {
                var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                return s ? +p.substr(0, 4) + +p.substr(4, 4) : p;
            }

            return _p8() + _p8(true) + _p8(true) + _p8();
        }

        var student = {
            "_id": guid(),
            "name": {
                "first": "",
                "last": ""
            },
            "age": 0,
            "group": 1,
            "gender": "m"
        };

        student.name.first = this.querySelector("#first-name").value;
        student.name.last = this.querySelector("#last-name").value;
        student.age = this.querySelector("#age").value;
        student.group = this.querySelector("#group").value;
        student.gender = this.gender.value;
        document.querySelector("#submitStudentForm").reset();
        people.push(student);
        modal.style.display = "none";
        ShowPeople();

    });

    document.querySelectorAll(".js-tab-links a").forEach(function (el) {
        el.addEventListener("click", function () {
            showTab(this.getAttribute("href"));
        })
    });

    function showTab(tabId) {
        hideAllTabs();
        document.querySelector(tabId).classList.remove("hidden");
        var link = document.querySelector(".js-tab-links a[href='" + tabId + "']");
        if (link != NaN) {
            link.parentNode.classList.add("active");
        }
    }

    function hideAllTabs() {
        document.querySelectorAll(".js-tab").forEach(function (el) {
            el.classList.add("hidden");
        });
        var link = document.querySelector(".js-tab-links li.active");
        if (link != NaN) {
            link.classList.remove("active");
        }
    }

    //Таблица + сортировка + отрисовка выбранных групп.
    var NumbGroup = "all";
    ShowPeople(NumbGroup);

    var HeaderTD = document.querySelectorAll("#studTab>thead td:not(:last-child)");

    HeaderTD.forEach(function (element) {
        element.addEventListener("click", headerClickHandler);
    });

    function headerClickHandler() {
        var direction = "desc";
        if (this.dataset["sort"] === "desc") {
            direction = "asc";
        }

        HeaderTD.forEach(function (element) {
            element.removeAttribute("data-sort")
        });

        this.setAttribute("data-sort", direction);
        sortPeople(this.dataset["prop"], direction);
        ShowPeople();
    }

    document.querySelectorAll("#groupType>ul li").forEach(function (element) {
        element.addEventListener("click", headerClickGroup);
    });

    function headerClickGroup() {
        NumbGroup = this.dataset["group"];
        ShowGroup(NumbGroup);
    }

    function ShowGroup(NumbGroup) {
        document.querySelectorAll('#studTab>tbody tr').forEach(function (el) {
            if (NumbGroup != "all") {
                el.classList.add("hidden");
            } else {
                document.querySelectorAll('#studTab>tbody tr').forEach(function (el) {
                    el.classList.remove("hidden");
                });
            }
        });

        document.querySelectorAll('#studTab>tbody td[data-group="' + NumbGroup + '"]').forEach(function (el) {
            el.parentNode.classList.remove("hidden");
        });
    }

    function ShowPeople() {
        var peopleL = people.length;
        var first;
        var last;
        var age;
        var numGroup;
        var gender;
        var tab = "";

        for (var i = 0; i < peopleL; i++) {
            var genders = {f: "Ж", m: "М"};
            first = people[i].name.first;
            last = people[i].name.last;
            age = people[i].age;
            numGroup = people[i].group;
            gender = genders[people[i].gender];
            tab += "<tr><td class='group' data-group='" + numGroup + "'> " + numGroup + " </td><td class='last'> "
                + last + " </td><td class='first'> " + first + " </td><td class='age'> " + age
                + " </td><td class='gender'> " + gender + " </td><td><a data-role='del-student' data-id='" + people[i]["_id"]
                + "' type='button' href='#' ><span class='glyphicon glyphicon-remove'></span></a> "
                + "<a data-role='modificationStudent' data-id='" + people[i]["_id"] + "' type='button' href='#'>"
                + "<span class='glyphicon glyphicon-pencil'></span></a></td></tr>";
        }
        document.getElementById("genTable").innerHTML = tab;
        ShowGroup(NumbGroup);

        document.querySelectorAll("[data-role='del-student']").forEach(function (delP) {
            delP.addEventListener("click", DelPeople)
        });
        document.querySelectorAll("[data-role='modificationStudent']").forEach(function (modP) {
            modP.addEventListener("click", modStudent)

        });
    }

    function modStudent(e) {
        e.preventDefault();
        var i;
        for (i = 0; i < people.length; i++) {
            if (people[i]["_id"] == this.dataset["id"]) {
                document.querySelector("#age").value = people[i].age;
                document.querySelector("#first-name").value = people[i].name.first;
                document.querySelector("#last-name").value = people[i].name.last;
                document.querySelector("#group").value = people[i].group;
                if (people[i].gender == "f") {
                    console.log(people[i].gender);
                    document.querySelector("#genderF").setAttribute("checked","");
                } else {
                    console.log(people[i].gender);
                    document.querySelector("#genderM").setAttribute("checked","");
                }
                break;
            }
        }
        modal.style.display = "block";

        document.querySelector("#submitStudentForm").addEventListener("submit", function (e) {

            e.preventDefault();
            people[i].name.first = this.querySelector("#first-name").value;
            people[i].name.last = this.querySelector("#last-name").value;
            people[i].age = this.querySelector("#age").value;
            people[i].group = this.querySelector("#group").value;
            people[i].gender = this.gender.value;
            document.querySelector("#submitStudentForm").reset();
            modal.style.display = "none";
            sortPeople("last","asc");
            ShowPeople();
            hideAllGroups();
        });

    }

    function DelPeople(e) {

        e.preventDefault();
        console.log(this.dataset["id"]);
        for (var i = 0; i < people.length; i++) {
            if (people[i]["_id"] == this.dataset["id"]) {
                people.splice(i, 1);
                break;
            }
        }
        ShowPeople();
        showAllGroups();
        hideAllGroups();
    }

    function sortPeople(prop, direction) {
        people.sort(cmpStudent);

        function cmpStudent(a, b) {
            var tmp = 0;
            if (prop === "first" || prop === "last") {
                tmp = a.name[prop].localeCompare(b.name[prop]);
            } else {
                if (a[prop] > b[prop]) {
                    tmp = 1;
                } else if (a[prop] < b[prop]) {
                    tmp = -1;
                } else {
                    tmp = 0;
                }
            }
            if (direction === "desc") {
                tmp = -tmp;
            }
            return tmp;
        }

    }

    //Число женщин и мужчин
    var ppfAll = 0;
    var ppmAll = 0;
    var sumAgAll = 0;

    for (var i = 0; i < groups.length; i++) {
        var ppg = 0;
        var sumAg = 0;
        var ppf = 0;
        var ppm = 0;
        var AgeGroupM = 0;
        var AgeGroupF = 0;
        for (var j = 0; j < people.length; j++) {
            if (people[j].group == groups[i]) {
                ppg++;
                if (people[j].gender == "f") {
                    ppf++;
                    ppfAll++;
                    AgeGroupF += people[j].age
                } else if (people[j].gender == "m") {
                    ppm++;
                    ppmAll++;
                    AgeGroupM += people[j].age
                }
                sumAg += people[j].age;
                sumAgAll += people[j].age;
            }
        }
        document.getElementById("NumGroup").innerHTML += "<tr><td>Группа: " + groups[i] + "</td><td>Всего студентов "
            + ppg + "</td><td>Средний возраст " + Math.round(sumAg / ppg) + "</td><td>Женщин " + ppf + "</td><td>Мужчин " + ppm +
            "</td><td>Средний возраст мужчин: " + Math.round(AgeGroupM / ppm) + "</td><td>Cредний возраст женщин: " + Math.round(AgeGroupF / ppf) + "</td></tr>";
    }

    document.getElementById("TotalWomen").innerHTML = "Женшин: " + ppfAll + " человек.";
    document.getElementById("TotalMen").innerHTML = "Мужчин: " + ppmAll + " человек.";
    document.getElementById("AverageMen").innerHTML = "В среднем мужчин: " + Math.round(ppmAll / groups.length) + " человек.";
    document.getElementById("AverageWomen").innerHTML = "В среднем женщин: " + Math.round(ppfAll / groups.length) + " человек.";
    document.getElementById("AverageByAge").innerHTML = "Средний возраст: " + Math.round(sumAgAll / sumP) + ".";
    document.getElementById("AverageNumberOfPeople").innerHTML = "Среднее количество: " + Math.round(people.length / groups.length) + " человек.";


    //Попытка сделать Modal    

    var modal = document.getElementById("UpDateStudent");
    var btnModal = document.getElementById("UDSB");
    var spanClose = document.getElementsByClassName("close")[0];

    btnModal.addEventListener("click", function () {
        modal.style.display = "block";

        document.querySelector("#submitStudentForm").addEventListener("submit", function (e) {

            e.preventDefault();

            function guid() {
                function _p8(s) {
                    var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                    return s ? +p.substr(0, 4) + +p.substr(4, 4) : p;
                }

                return _p8() + _p8(true) + _p8(true) + _p8();
            }

            var student = {
                "_id": guid(),
                "name": {
                    "first": "",
                    "last": ""
                },
                "age": 0,
                "group": 1,
                "gender": "m"
            };

            student.name.first = this.querySelector("#first-name").value;
            student.name.last = this.querySelector("#last-name").value;
            student.age = this.querySelector("#age").value;
            student.group = this.querySelector("#group").value;
            student.gender = this.gender.value;
            document.querySelector("#submitStudentForm").reset();
            people.push(student);
            modal.style.display = "none";
            ShowPeople();
            hideAllGroups();
        });

    });

    spanClose.addEventListener("click", function () {
        modal.style.display = "none";
    });

    window.addEventListener("click", function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    });

}